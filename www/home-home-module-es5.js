function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html":
  /*!***************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html ***!
    \***************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppHomeHomePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  \n  <ion-toolbar>\n\n    <ion-title>\n      Pokédex {{ this.length }} / {{ this.maxLength }}\n    </ion-title>\n\n    <ion-buttons slot=\"end\">\n\n      <ion-button color=\"secondary\" (click)=\"showFilter()\">\n        <ion-icon name=\"filter-outline\"></ion-icon>\n      </ion-button>\n\n    </ion-buttons>\n    \n  </ion-toolbar>\n\n</ion-header>\n\n<ion-content>\n    \n  <!-- Lista de Pokemon -->\n  <ion-list lines=\"full\">\n    <ion-item *ngFor=\"let pokemon of pokemonArray\" (click)=\"loadPokemonDetail(pokemon.id)\">\n      <img class=\"pokemon-img\" [src]=\"'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/' + pokemon.id + '.png'\">\n      <ion-label>{{ pokemon.nombre }}</ion-label>\n    </ion-item>\n  </ion-list>\n\n  <ion-infinite-scroll threshold=\"100px\" (ionInfinite)=\"loadMorePokemon($event)\">\n    <ion-infinite-scroll-content\n      loadingSpinner=\"bubbles\"\n      loadingText=\"Localizando más pokemon...\">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n\n  <!-- Scroll to top -->\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n    <ion-fab-button (click)=\"scrollToTop()\">\n      <ion-icon name=\"arrow-up\"></ion-icon>\n    </ion-fab-button>\n  </ion-fab>\n\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/home/home-routing.module.ts":
  /*!*********************************************!*\
    !*** ./src/app/home/home-routing.module.ts ***!
    \*********************************************/

  /*! exports provided: HomePageRoutingModule */

  /***/
  function srcAppHomeHomeRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function () {
      return HomePageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./home.page */
    "./src/app/home/home.page.ts");

    var routes = [{
      path: '',
      component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"]
    }];

    var HomePageRoutingModule = function HomePageRoutingModule() {
      _classCallCheck(this, HomePageRoutingModule);
    };

    HomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], HomePageRoutingModule);
    /***/
  },

  /***/
  "./src/app/home/home.module.ts":
  /*!*************************************!*\
    !*** ./src/app/home/home.module.ts ***!
    \*************************************/

  /*! exports provided: HomePageModule */

  /***/
  function srcAppHomeHomeModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomePageModule", function () {
      return HomePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _home_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./home.page */
    "./src/app/home/home.page.ts");
    /* harmony import */


    var _home_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./home-routing.module */
    "./src/app/home/home-routing.module.ts");

    var HomePageModule = function HomePageModule() {
      _classCallCheck(this, HomePageModule);
    };

    HomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"], _home_routing_module__WEBPACK_IMPORTED_MODULE_6__["HomePageRoutingModule"]],
      declarations: [_home_page__WEBPACK_IMPORTED_MODULE_5__["HomePage"]]
    })], HomePageModule);
    /***/
  },

  /***/
  "./src/app/home/home.page.scss":
  /*!*************************************!*\
    !*** ./src/app/home/home.page.scss ***!
    \*************************************/

  /*! exports provided: default */

  /***/
  function srcAppHomeHomePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-toolbar {\n  box-shadow: 0px 0px 5px 2px rgba(0, 0, 0, 0.25);\n}\nion-toolbar ion-title {\n  padding-left: 0.55em;\n  text-align: left;\n}\nion-list ion-item .pokemon-img {\n  padding-right: 20px;\n}\nion-infinite-scroll {\n  margin-top: 25px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yZ2FyY2lhZGVsb25nb3JpYS9EZXNrdG9wL1Byb3llY3Rvcy9wb2tlZGV4LXByby9zcmMvYXBwL2hvbWUvaG9tZS5wYWdlLnNjc3MiLCJzcmMvYXBwL2hvbWUvaG9tZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFJRSwrQ0FBQTtBQ0FGO0FERUU7RUFDRSxvQkFBQTtFQUNBLGdCQUFBO0FDQUo7QURNSTtFQUNFLG1CQUFBO0FDSE47QURRQTtFQUNFLGdCQUFBO0FDTEYiLCJmaWxlIjoic3JjL2FwcC9ob21lL2hvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXRvb2xiYXIge1xuXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDBweCA1cHggMnB4IHJnYmEoMCwwLDAsMC4yNSk7XG4gIC1tb3otYm94LXNoYWRvdzogMHB4IDBweCA1cHggMnB4IHJnYmEoMCwwLDAsMC4yNSk7XG4gIGJveC1zaGFkb3c6IDBweCAwcHggNXB4IDJweCByZ2JhKDAsMCwwLDAuMjUpO1xuXG4gIGlvbi10aXRsZSB7XG4gICAgcGFkZGluZy1sZWZ0OiAwLjU1ZW07XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgfVxufVxuXG5pb24tbGlzdCB7XG4gIGlvbi1pdGVtIHtcbiAgICAucG9rZW1vbi1pbWcge1xuICAgICAgcGFkZGluZy1yaWdodDogMjBweDtcbiAgICB9XG4gIH1cbn1cblxuaW9uLWluZmluaXRlLXNjcm9sbCB7XG4gIG1hcmdpbi10b3A6IDI1cHg7XG59IiwiaW9uLXRvb2xiYXIge1xuICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCAwcHggNXB4IDJweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xuICAtbW96LWJveC1zaGFkb3c6IDBweCAwcHggNXB4IDJweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xuICBib3gtc2hhZG93OiAwcHggMHB4IDVweCAycHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcbn1cbmlvbi10b29sYmFyIGlvbi10aXRsZSB7XG4gIHBhZGRpbmctbGVmdDogMC41NWVtO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufVxuXG5pb24tbGlzdCBpb24taXRlbSAucG9rZW1vbi1pbWcge1xuICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xufVxuXG5pb24taW5maW5pdGUtc2Nyb2xsIHtcbiAgbWFyZ2luLXRvcDogMjVweDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/home/home.page.ts":
  /*!***********************************!*\
    !*** ./src/app/home/home.page.ts ***!
    \***********************************/

  /*! exports provided: HomePage */

  /***/
  function srcAppHomeHomePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomePage", function () {
      return HomePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
    /* harmony import */


    var _services_poke_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../services/poke-api.service */
    "./src/app/services/poke-api.service.ts");
    /* harmony import */


    var _filter_filter_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../filter/filter.page */
    "./src/app/filter/filter.page.ts");
    /* harmony import */


    var _detail_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../detail/detail.page */
    "./src/app/detail/detail.page.ts");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");

    var HomePage = /*#__PURE__*/function () {
      /*****************
       * Constructores *
       *****************/
      function HomePage(modalController, toastController, _pokeApiService, _storage) {
        var _this = this;

        _classCallCheck(this, HomePage);

        this.modalController = modalController;
        this.toastController = toastController;
        this._pokeApiService = _pokeApiService;
        this._storage = _storage;
        /***************
         * Propiedades *
         ***************/

        this._offset = 0;
        this._limit = 50;
        this.pokemonArray = [];
        this.maxLength = 0;
        this.length = 0;
        /*
        Nota: Ejemplo sencillo de uso de Subscription y Subject, en otros proyectos
        lo he usado para, desde un servicio central, crear la posibilidad de que distintos
        componentes que no se conocen puedan saber cuando se desencadena una acción, por ejemplo
        cuando se recibe una notificación Push o cuando el usuario realiza login. En el caso del
        login, si un compañero crease un componente que depende de si el usuario esta o no logado
        (estado estático) puede saber cuando ha cambiado este estado subscribiendose a un evento
        general y reaccionar en tiempo real.
               Aqui hago algo un poco absurdo, solo para dejar constancia de que lo conozco y lo uso.
        */

        this.scrollToTopSource = new rxjs__WEBPACK_IMPORTED_MODULE_7__["Subject"]();
        this.scrollToTop$ = this.scrollToTopSource.asObservable(); // Uso $ al final del observable, no lo he usado en más sitios por que no se si lo consideráis buena practica

        /*
        Reaccionar ante las llamadas del evento .next del Subject llamado scrollToTopSource
        mediante la subscripción al observable scrollToTop$
        */

        this.subscriptionScrollToTop = this.scrollToTop$.subscribe(function (data) {
          _this.presentToast('Bienvenido de a lo más alto');
        });
      }
      /***********
       * Eventos *
       ***********/


      _createClass(HomePage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this._storage.remove("filter");
        }
      }, {
        key: "ngAfterViewInit",
        value: function ngAfterViewInit() {
          this.loadMorePokemon(null);
        }
        /***********
         * Métodos *
         ***********/

        /**
         * Mueve el scroll al Top
         */

      }, {
        key: "scrollToTop",
        value: function scrollToTop() {
          this.content.scrollToTop(500); // Lanzar el evento .next del Subject llamado scrollToTopSource

          this.scrollToTopSource.next('Se ha hecho scrollToTop');
        }
        /**
         * Abre el detalle del pokemon seleccionado
         *
         * @param data datos del pokemon
         */

      }, {
        key: "showDetail",
        value: function showDetail(data) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var modal;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.modalController.create({
                      component: _detail_detail_page__WEBPACK_IMPORTED_MODULE_6__["DetailPage"],
                      componentProps: {
                        data: data
                      }
                    });

                  case 2:
                    modal = _context.sent;
                    modal.onDidDismiss().then(function (detail) {});
                    _context.next = 6;
                    return modal.present();

                  case 6:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
        /**
         * Abre el filtro modal
         */

      }, {
        key: "showFilter",
        value: function showFilter() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var _this2 = this;

            var modal;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.modalController.create({
                      component: _filter_filter_page__WEBPACK_IMPORTED_MODULE_5__["FilterPage"],
                      componentProps: {
                        aParameter: true,
                        otherParameter: new Date()
                      }
                    });

                  case 2:
                    modal = _context2.sent;
                    modal.onDidDismiss().then(function (detail) {
                      var self = _this2;

                      if (detail !== null && detail.hasOwnProperty('data') && detail['data'] > 0) {
                        /*
                        Caso en el que cargamos los pokemon filtrados
                        */
                        _this2.content.scrollToTop();

                        _this2.pokemonArray = [];

                        _this2._pokeApiService.getPokemonType(detail.data).subscribe(function (data) {
                          self.maxLength = data.hasOwnProperty('pokemon') ? data['pokemon'].length : self.maxLength;
                          self.length = self.maxLength;

                          if (self.maxLength > 0) {
                            self.infiniteScroll.disabled = true;
                          } // Poblar el array de pokemon con los pokemon conseguidos


                          data['pokemon'].forEach(function (element) {
                            var pokemon = {
                              id: '',
                              nombre: ''
                            };
                            var urlSplit = element['pokemon']['url'].split('/');
                            pokemon.id = urlSplit[urlSplit.length - 2];
                            pokemon.nombre = element['pokemon']['name'];
                            self.pokemonArray.push(pokemon);
                          });
                        }, function (error) {
                          console.log(error);
                        });
                      } else if (detail !== null && detail.hasOwnProperty('data') && detail['data'] == -1) {
                        /*
                        Caso en el que cerramos la modal de filtro sin cambios
                        */
                        // No hacer nada se ha cerrado sin modificar
                      } else {
                        /*
                        Caso en el que deshacemos el filtro existente
                        */
                        _this2.content.scrollToTop();

                        _this2._offset = 0;
                        _this2.pokemonArray = [];
                        _this2.infiniteScroll.disabled = false;

                        _this2.loadMorePokemon(null);
                      }
                    });
                    _context2.next = 6;
                    return modal.present();

                  case 6:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
        /**
         * Recupera los datos de detalle de un pokemon
         *
         * @param id
         */

      }, {
        key: "loadPokemonDetail",
        value: function loadPokemonDetail() {
          var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var _this3 = this;

            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    this._pokeApiService.getPokemonDetail(id).subscribe(function (data) {
                      var pokemon = {
                        id: data["id"],
                        nombre: data["name"],
                        sprite_back_default: data["sprites"]["back_default"],
                        sprite_back_female: data["sprites"]["back_female"],
                        sprite_back_shiny: data["sprites"]["back_shiny"],
                        sprite_back_shiny_female: data["sprites"]["back_shiny_female"],
                        sprite_front_default: data["sprites"]["front_default"],
                        sprite_front_female: data["sprites"]["front_female"],
                        sprite_front_shiny: data["sprites"]["front_shiny"],
                        sprite_front_shiny_female: data["sprites"]["front_shiny_female"],
                        base_experience: data["base_experience"],
                        height: data["height"],
                        forms: data["forms"],
                        moves: data["moves"],
                        types: data["types"]
                      };

                      _this3.showDetail(pokemon);
                    }, function (error) {
                      console.log(error);
                    });

                  case 1:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
        /**
         * Recupera más pokemon
         *
         * @param event
         */

      }, {
        key: "loadMorePokemon",
        value: function loadMorePokemon(event) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
            var _this4 = this;

            var self;
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    self = this;
                    setTimeout(function () {
                      // Incrementar el offset según el limite establecido para cada carga
                      self._offset = self._offset + self._limit;

                      _this4._pokeApiService.getPokemon(self._offset, self._limit).subscribe(function (data) {
                        self.maxLength = data['count']; // Poblar el array de pokemon con los pokemon conseguidos

                        data['results'].forEach(function (element) {
                          var pokemon = {
                            id: '',
                            nombre: ''
                          };
                          var urlSplit = element['url'].split('/');
                          pokemon.id = urlSplit[urlSplit.length - 2];
                          pokemon.nombre = element['name'];
                          self.pokemonArray.push(pokemon);
                        });

                        if (event) {
                          event.target.complete(); // Desactivar la posibilidad de cargar más pokemon al llegar a los 1000 elementos

                          if (self.pokemonArray.length >= 1000) {
                            event.target.disabled = true;
                          }
                        } // Controlar la cantidad de pokemon conseguidos


                        self.length = self.pokemonArray.length;
                      }, function (error) {
                        console.log(error);
                      });
                    }, 500);

                  case 2:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));
        }
        /**
         * Muestra un toast con un mensaje
         *
         * @param message mensaje a mostrar
         */

      }, {
        key: "presentToast",
        value: function presentToast() {
          var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
            var toast;
            return regeneratorRuntime.wrap(function _callee5$(_context5) {
              while (1) {
                switch (_context5.prev = _context5.next) {
                  case 0:
                    _context5.next = 2;
                    return this.toastController.create({
                      message: message,
                      duration: 2000
                    });

                  case 2:
                    toast = _context5.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context5.stop();
                }
              }
            }, _callee5, this);
          }));
        }
      }]);

      return HomePage;
    }();

    HomePage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
      }, {
        type: _services_poke_api_service__WEBPACK_IMPORTED_MODULE_4__["PokeApiService"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"]
      }];
    };

    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonInfiniteScroll"])], HomePage.prototype, "infiniteScroll", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonContent"])], HomePage.prototype, "content", void 0);
    HomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-home',
      template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! raw-loader!./home.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html"))["default"],
      styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! ./home.page.scss */
      "./src/app/home/home.page.scss"))["default"]]
    })], HomePage);
    /***/
  }
}]);
//# sourceMappingURL=home-home-module-es5.js.map
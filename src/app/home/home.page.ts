import { Component, ViewChild } from '@angular/core';

import { IonInfiniteScroll, IonContent, ToastController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

import { PokemonInterface } from '../interfaces/pokemon';
import { PokemonDetailInterface } from '../interfaces/pokemon-detail';
import { PokeApiService } from '../services/poke-api.service';
import { FilterPage } from '../filter/filter.page';
import { DetailPage } from '../detail/detail.page';
import { Subscription, Subject } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  /***************
   * Propiedades *
   ***************/

  private _offset: number = 0;
  private _limit: number = 50;

  public pokemonArray: PokemonInterface[] = [];
  public maxLength: number = 0;
  public length: number = 0;

  /*
  Nota: Ejemplo sencillo de uso de Subscription y Subject, en otros proyectos
  lo he usado para, desde un servicio central, crear la posibilidad de que distintos 
  componentes que no se conocen puedan saber cuando se desencadena una acción, por ejemplo
  cuando se recibe una notificación Push o cuando el usuario realiza login. En el caso del 
  login, si un compañero crease un componente que depende de si el usuario esta o no logado
  (estado estático) puede saber cuando ha cambiado este estado subscribiendose a un evento
  general y reaccionar en tiempo real.

  Aqui hago algo un poco absurdo, solo para dejar constancia de que lo conozco y lo uso.
  */
  public scrollToTopSource = new Subject<any>();
  public scrollToTop$ = this.scrollToTopSource.asObservable(); // Uso $ al final del observable, no lo he usado en más sitios por que no se si lo consideráis buena practica
  public subscriptionScrollToTop: Subscription;

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild(IonContent) content: IonContent;

  /*****************
   * Constructores *
   *****************/

  constructor(
    public modalController: ModalController,
    public toastController: ToastController,
    private _pokeApiService: PokeApiService,
    private _storage: Storage
  ) {

    /*
    Reaccionar ante las llamadas del evento .next del Subject llamado scrollToTopSource
    mediante la subscripción al observable scrollToTop$
    */
    this.subscriptionScrollToTop = this.scrollToTop$.subscribe(

      (data: any) => {
        this.presentToast('Bienvenido de a lo más alto')
      }
    );
  }

  /***********
   * Eventos *
   ***********/

  ngOnInit() {
    
    this._storage.remove("filter");
  }

  ngAfterViewInit() {

    this.loadMorePokemon(null);
  }

  /***********
   * Métodos *
   ***********/

  /**
   * Mueve el scroll al Top
   */
  scrollToTop() {
      
    this.content.scrollToTop(500);

    // Lanzar el evento .next del Subject llamado scrollToTopSource
    this.scrollToTopSource.next('Se ha hecho scrollToTop');
  }

  /**
   * Abre el detalle del pokemon seleccionado
   * 
   * @param data datos del pokemon
   */
  async showDetail(data: PokemonDetailInterface) {

    const modal: HTMLIonModalElement =
       await this.modalController.create({
          component: DetailPage,
          componentProps: {
             data: data
          }
    });
     
    modal.onDidDismiss().then((detail: any) => {

    });
    
    await modal.present();
  }

  /**
   * Abre el filtro modal
   */
  async showFilter() {
    const modal: HTMLIonModalElement =
       await this.modalController.create({
          component: FilterPage,
          componentProps: {
             aParameter: true,
             otherParameter: new Date()
          }
    });
     
    modal.onDidDismiss().then((detail: any) => {

      let self = this;

      if (
        (detail !== null) && 
        (detail.hasOwnProperty('data')) && 
        (detail['data'] > 0)
        ) {

        /*
        Caso en el que cargamos los pokemon filtrados
        */

        this.content.scrollToTop();

        this.pokemonArray = [];

        this._pokeApiService.getPokemonType(detail.data).subscribe(data => {
          
          self.maxLength = (data.hasOwnProperty('pokemon')) ? data['pokemon'].length : self.maxLength;
          self.length = self.maxLength;

          if (self.maxLength > 0) {
            self.infiniteScroll.disabled = true;
          }

          // Poblar el array de pokemon con los pokemon conseguidos
          data['pokemon'].forEach(element => {
            
            let pokemon: PokemonInterface = {
              id: '',
              nombre: ''
            };

            let urlSplit = element['pokemon']['url'].split('/');
      
            pokemon.id = urlSplit[urlSplit.length - 2];

            pokemon.nombre = element['pokemon']['name'];
      
            self.pokemonArray.push(pokemon);
          });

        }, error => {
          console.log(error);
        });
      } else if (
        (detail !== null) && 
        (detail.hasOwnProperty('data')) && 
        (detail['data'] == -1)
      ) {

        /*
        Caso en el que cerramos la modal de filtro sin cambios
        */

        // No hacer nada se ha cerrado sin modificar

      } else {

        /*
        Caso en el que deshacemos el filtro existente
        */

        this.content.scrollToTop();

        this._offset = 0;

        this.pokemonArray = [];

        this.infiniteScroll.disabled = false;

        this.loadMorePokemon(null);
      }
    });
    
    await modal.present();
  }

  /**
   * Recupera los datos de detalle de un pokemon
   * 
   * @param id 
   */
  async loadPokemonDetail(id: number = 0) {

    this._pokeApiService.getPokemonDetail(id).subscribe(data => {

      let pokemon: PokemonDetailInterface = {
        id: data["id"],
        nombre: data["name"],
        sprite_back_default: data["sprites"]["back_default"],
        sprite_back_female: data["sprites"]["back_female"],
        sprite_back_shiny: data["sprites"]["back_shiny"],
        sprite_back_shiny_female: data["sprites"]["back_shiny_female"],
        sprite_front_default: data["sprites"]["front_default"],
        sprite_front_female: data["sprites"]["front_female"],
        sprite_front_shiny: data["sprites"]["front_shiny"],
        sprite_front_shiny_female: data["sprites"]["front_shiny_female"],
        base_experience: data["base_experience"],
        height: data["height"],
        forms: data["forms"],
        moves: data["moves"],
        types: data["types"],
      };

      this.showDetail(pokemon);
    }, error => {
      console.log(error);
    });
  }

  /**
   * Recupera más pokemon
   * 
   * @param event 
   */
  async loadMorePokemon(event) {

    let self = this;

    setTimeout(() => {

      // Incrementar el offset según el limite establecido para cada carga
      self._offset = self._offset + self._limit;

      this._pokeApiService.getPokemon(self._offset, self._limit).subscribe(data => {

        self.maxLength = data['count'];

        // Poblar el array de pokemon con los pokemon conseguidos
        data['results'].forEach(element => {
          
          let pokemon: PokemonInterface = {
            id: '',
            nombre: ''
          };

          let urlSplit = element['url'].split('/');
    
          pokemon.id = urlSplit[urlSplit.length - 2];

          pokemon.nombre = element['name'];
    
          self.pokemonArray.push(pokemon);
        });

        if (event) {
          
          event.target.complete();

          // Desactivar la posibilidad de cargar más pokemon al llegar a los 1000 elementos
          if (self.pokemonArray.length >= 1000) {
            event.target.disabled = true;
          }
        }

        // Controlar la cantidad de pokemon conseguidos
        self.length = self.pokemonArray.length;
  
      }, error => {
        console.log(error);
      });
    }, 500);
  }

  /**
   * Muestra un toast con un mensaje
   * 
   * @param message mensaje a mostrar
   */
  async presentToast(message: string = '') {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }
}

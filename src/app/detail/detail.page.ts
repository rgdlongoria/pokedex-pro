import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

import { PokemonDetailInterface } from '../interfaces/pokemon-detail';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {

  /***************
   * Propiedades *
   ***************/

  public pokemon: PokemonDetailInterface = {
    id: '',
    nombre: '',
    sprite_back_default: '',
    sprite_back_female: '',
    sprite_back_shiny: '',
    sprite_back_shiny_female: '',
    sprite_front_default: '',
    sprite_front_female: '',
    sprite_front_shiny: '',
    sprite_front_shiny_female: '',
    base_experience: '',
    height: '',
    forms: [],
    moves: [],
    types: []
  };

  /*****************
   * Constructores *
   *****************/

  constructor(
    private _modalController: ModalController,
    private _navParams: NavParams
  ) {
  }

  /***********
   * Métodos *
   ***********/

  ngOnInit() {
  }

  /**
   * Logica a ejecutar al entrar en la vista
   */
  async ionViewWillEnter() {

    this.pokemon = this._navParams.get("data");
  }

  /**
   * Logica a ejecutar al cerrar la modal
   * 
   * @param id identificador del pokemon
   */
  async closeModal(id: number = 0) {
    
    await this._modalController.dismiss(id);
  }

}

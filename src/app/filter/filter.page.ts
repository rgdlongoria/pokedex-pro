import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

import { PokeApiService } from '../services/poke-api.service';
import { PokemonTypeInterface } from '../interfaces/pokemon-type';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.page.html',
  styleUrls: ['./filter.page.scss'],
})
export class FilterPage implements OnInit {

  /***************
   * Propiedades *
   ***************/

  private _typeSelected: number = 0;

  public pokemonTypeArray: PokemonTypeInterface[] = [];

  /*****************
   * Constructores *
   *****************/

  constructor(
    private _modalController: ModalController,
    private _navParams: NavParams,
    private _pokeApiService: PokeApiService,
    private _storage: Storage
  ) {
  }

  /***********
   * Eventos *
   ***********/

  ngOnInit() {

  }

  /***********
   * Métodos *
   ***********/

  /**
   * Logica a ejecutar al entrar en la vista
   */
  async ionViewWillEnter() {

    this._typeSelected = await this._storage.get('filter');

    this.loadPokemonType();
  }

  /**
   * Logica a ejecutar al cerrar la modal
   * 
   * @param id identificador del tipo de pokemon
   */
  async closeModal(id: number = 0) {
    
    await this._modalController.dismiss(id);
  }

  /**
   * Guardar como dato persistente el filtro aplicado o la omisión del mismo
   * 
   * @param id identificador del tipo de pokemon
   */
  applyFilter(id: number = 0) {

    if (this._typeSelected == id) {

      this._storage.remove('filter');

      id = undefined;

    } else {

      this._storage.set('filter',id);
    }

    this.closeModal(id);
  }

  /**
   * Cargar los tipos de pokemon por los que se puede filtrar
   */
  async loadPokemonType() {

    let self = this;

    setTimeout(() => {

      this._pokeApiService.getPokemonType().subscribe(data => {

        // Poblar el array de pokemon con los pokemon conseguidos
        data['results'].forEach(element => {
          
          let pokemonType: PokemonTypeInterface = {
            id: '',
            nombre: '',
            selected: false
          };

          let urlSplit = element['url'].split('/');
    
          pokemonType.id = urlSplit[urlSplit.length - 2];

          pokemonType.nombre = element['name'];

          if (+pokemonType.id == this._typeSelected) {
            pokemonType.selected = true;
            self.pokemonTypeArray.unshift(pokemonType); // El elemento seleccionado sube a la primera posición para reconocerlo facilmente
          } else {
            self.pokemonTypeArray.push(pokemonType);
          }
        });
  
      }, error => {
        console.log(error);
      });
    }, 500);
  }
}

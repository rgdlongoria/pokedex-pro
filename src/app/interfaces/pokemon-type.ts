export interface PokemonTypeInterface {
  id: String;
  nombre: String;
  selected: boolean
}

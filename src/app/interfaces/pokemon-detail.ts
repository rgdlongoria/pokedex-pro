export interface PokemonDetailInterface {
  id: String;
  nombre: String;
  sprite_back_default: String;
  sprite_back_female: String;
  sprite_back_shiny: String;
  sprite_back_shiny_female: String;
  sprite_front_default: String;
  sprite_front_female: String;
  sprite_front_shiny: String;
  sprite_front_shiny_female: String;
  base_experience: String;
  height: String;
  forms: Array<Object>;
  moves: Array<Object>;
  types: Array<Object>;
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PokeApiService {

  /***************
   * Propiedades *
   ***************/

  private _baseUrl: string = 'https://pokeapi.co/api/v2';

  /*****************
   * Constructores *
   *****************/

  constructor(
    private _http: HttpClient
  ) {}

  /***********
   * Métodos *
   ***********/

  /**
   * Obtener un listado de pokemon
   * 
   * @param offset número de elementos iniciales descartados
   * @param limit límite de elementos a mostrar
   */
  getPokemon(offset: number = 0, limit: number = 25) {
    return this._http.get(`${this._baseUrl}/pokemon?offset=${offset}&limit=${limit}`).pipe(
      map(result => {
        return result;
      })
    );
  }

  /**
   * Obtener el detalle de un pokemon
   * 
   * @param id identificador del pokemon
   */
  getPokemonDetail(id: number = 0) {
    return this._http.get(`${this._baseUrl}/pokemon/${id}`).pipe(
      map(result => {
        return result;
      })
    );
  }

  /**
   * Obtener un listado de pokemon por tipo
   * 
   * @param id 
   */
  getPokemonType(id?: number) {

    let url = `${this._baseUrl}/type`;

    if (id) url = url + `/${id}`;

    return this._http.get(url).pipe(
      map(result => {
        return result;
      })
    );
  }
}
